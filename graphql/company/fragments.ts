import gql from "graphql-tag";

export const CompanyFragment = gql`
  fragment CompanyFragment on Company {
    id
    title
    logo
  }
`;
