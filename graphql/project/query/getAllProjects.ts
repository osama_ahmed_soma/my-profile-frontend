import gql from "graphql-tag";
import { ProjectFragment } from "../fragments";

export const getAllStackQuery = gql`
  query GetAllProject {
    getAllProject {
      ...ProjectFragment
    }
  }

  ${ProjectFragment}
`;
