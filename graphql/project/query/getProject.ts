import gql from "graphql-tag";
import { ProjectFragment } from "../fragments";

export const GetProjectQuery = gql`
  query GetProject($id: String!) {
    getProject(id: $id) {
      ...ProjectFragment
      tagLine
      url
      completionTime
      sourceCode
      description_point
    }
  }
  ${ProjectFragment}
`;
