import gql from "graphql-tag";
import { SkillIDTitleFragment } from "../skill/fragments";
import { CompanyFragment } from "../company/fragments";

export const ProjectMinifyFragment = gql`
  fragment ProjectMinifyFragment on Project {
    id
    title
    description
    skills {
      ...SkillIDTitle
    }
  }
  ${SkillIDTitleFragment}
`;

export const ProjectFragment = gql`
  fragment ProjectFragment on Project {
    ...ProjectMinifyFragment
    image
    work_experience {
      company {
        ...CompanyFragment
      }
    }
  }
  ${ProjectMinifyFragment}
  ${CompanyFragment}
`;
