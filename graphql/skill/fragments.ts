import gql from "graphql-tag";

export const SkillIDTitleFragment = gql`
  fragment SkillIDTitle on Skill {
    id
    title
  }
`;
