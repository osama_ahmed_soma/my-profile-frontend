import gql from "graphql-tag";
import { SkillIDTitleFragment } from "../fragments";

export const GetMainSkills = gql`
  query GetMainSkills($isMain: Boolean) {
    getMainSkills(isMain: $isMain) {
      ...SkillIDTitle
    }
  }
  ${SkillIDTitleFragment}
`;
