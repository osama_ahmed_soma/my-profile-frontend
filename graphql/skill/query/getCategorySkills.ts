import gql from "graphql-tag";
import { SkillIDTitleFragment } from "../fragments";

export const GetCategorySkills = gql`
  query GetCategorySkills {
    getCategorySkills {
      ...SkillIDTitle
    }
  }
  ${SkillIDTitleFragment}
`;
