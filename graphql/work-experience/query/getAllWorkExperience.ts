import gql from "graphql-tag";
import { SkillIDTitleFragment } from "../../skill/fragments";
import { CompanyFragment } from "../../company/fragments";
import { ProjectMinifyFragment } from "../../project/fragments";

export const GetAllWorkExperience = gql`
  query GetAllWorkExperience {
    getAllWorkExperience {
      id
      title
      duration {
        start
        end
        present
      }
      company {
        ...CompanyFragment
      }
      description
      projects {
        ...ProjectMinifyFragment
        description_point
      }
      skills {
        ...SkillIDTitle
      }
      notes
    }
  }
  ${SkillIDTitleFragment}
  ${CompanyFragment}
  ${ProjectMinifyFragment}
`;
