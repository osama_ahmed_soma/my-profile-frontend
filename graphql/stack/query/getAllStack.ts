import gql from "graphql-tag";
import { SkillIDTitleFragment } from "../../skill/fragments";

export const getAllStackQuery = gql`
  query GetAllStack($filters: StackSkillsFilterInput) {
    getAllStack {
      id
      title
      image
      skills(filters: $filters) {
        ...SkillIDTitle
        rating
      }
    }
  }
  ${SkillIDTitleFragment}
`;
