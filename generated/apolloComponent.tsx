export type Maybe<T> = T | null;

export interface StackSkillsFilterInput {
  isMain?: Maybe<boolean>;
}

export interface CreateWorkExperienceInput {
  title: string;

  duration: DurationInput;

  companyID: string;

  description: string;

  notes?: Maybe<string>;
}

export interface DurationInput {
  start: DateTime;

  end?: Maybe<DateTime>;

  present?: Maybe<boolean>;
}

export interface CreateStackInput {
  title: string;

  image?: Maybe<string>;
}

export interface CreateSkillInput {
  title: string;

  rating?: Maybe<number>;

  isMain?: Maybe<boolean>;

  isCategory?: Maybe<boolean>;
}

export interface CreateProjectInput {
  title: string;

  description: string;

  description_point?: Maybe<string[]>;

  image?: Maybe<string>;

  tagLine?: Maybe<string>;

  url?: Maybe<string>;

  completionTime?: Maybe<string>;

  sourceCode?: Maybe<string>;

  weID: string;

  isFeatured?: Maybe<boolean>;
}

export interface CreateCompanyInput {
  title: string;

  logo?: Maybe<string>;
}

export interface ContactInput {
  name: string;

  email: string;

  message: string;
}

export interface AssignProjectSkillsInput {
  skillIDs: string[];

  projectID: string;
}

export interface AssignSkillStacksInput {
  skillID: string;

  stackIDs: string[];
}

export interface AssignStackSkillsInput {
  skillIDs: string[];

  stackID: string;
}

export interface AssignWorkExperienceSkillsInput {
  skillIDs: string[];

  weID: string;
}

/** The javascript `Date` as string. Type represents date and time as the ISO Date string. */
export type DateTime = any;

// ====================================================
// Documents
// ====================================================

export type ContactVariables = {
  data: ContactInput;
};

export type ContactMutation = {
  __typename?: "Mutation";

  contact: boolean;
};

export type GetAllProjectVariables = {};

export type GetAllProjectQuery = {
  __typename?: "Query";

  getAllProject: GetAllProjectGetAllProject[];
};

export type GetAllProjectGetAllProject = ProjectFragmentFragment;

export type GetFeaturedProjectsVariables = {};

export type GetFeaturedProjectsQuery = {
  __typename?: "Query";

  getFeaturedProjects: GetFeaturedProjectsGetFeaturedProjects[];
};

export type GetFeaturedProjectsGetFeaturedProjects = ProjectFragmentFragment;

export type GetProjectVariables = {
  id: string;
};

export type GetProjectQuery = {
  __typename?: "Query";

  getProject: GetProjectGetProject;
};

export type GetProjectGetProject = {
  __typename?: "Project";

  tagLine: Maybe<string>;

  url: Maybe<string>;

  completionTime: Maybe<string>;

  sourceCode: Maybe<string>;

  description_point: Maybe<string[]>;
} & ProjectFragmentFragment;

export type GetCategorySkillsVariables = {};

export type GetCategorySkillsQuery = {
  __typename?: "Query";

  getCategorySkills: GetCategorySkillsGetCategorySkills[];
};

export type GetCategorySkillsGetCategorySkills = SkillIdTitleFragment;

export type GetMainSkillsVariables = {
  isMain?: Maybe<boolean>;
};

export type GetMainSkillsQuery = {
  __typename?: "Query";

  getMainSkills: GetMainSkillsGetMainSkills[];
};

export type GetMainSkillsGetMainSkills = SkillIdTitleFragment;

export type GetAllStackVariables = {
  filters?: Maybe<StackSkillsFilterInput>;
};

export type GetAllStackQuery = {
  __typename?: "Query";

  getAllStack: GetAllStackGetAllStack[];
};

export type GetAllStackGetAllStack = {
  __typename?: "Stack";

  id: string;

  title: string;

  image: Maybe<string>;

  skills: GetAllStackSkills[];
};

export type GetAllStackSkills = {
  __typename?: "Skill";

  rating: number;
} & SkillIdTitleFragment;

export type GetAllWorkExperienceVariables = {};

export type GetAllWorkExperienceQuery = {
  __typename?: "Query";

  getAllWorkExperience: GetAllWorkExperienceGetAllWorkExperience[];
};

export type GetAllWorkExperienceGetAllWorkExperience = {
  __typename?: "WorkExperience";

  id: string;

  title: string;

  duration: GetAllWorkExperienceDuration;

  company: GetAllWorkExperienceCompany;

  description: string;

  projects: Maybe<GetAllWorkExperienceProjects[]>;

  skills: GetAllWorkExperienceSkills[];

  notes: Maybe<string>;
};

export type GetAllWorkExperienceDuration = {
  __typename?: "Duration";

  start: DateTime;

  end: Maybe<DateTime>;

  present: Maybe<boolean>;
};

export type GetAllWorkExperienceCompany = CompanyFragmentFragment;

export type GetAllWorkExperienceProjects = {
  __typename?: "Project";

  description_point: Maybe<string[]>;
} & ProjectMinifyFragmentFragment;

export type GetAllWorkExperienceSkills = SkillIdTitleFragment;

export type CompanyFragmentFragment = {
  __typename?: "Company";

  id: string;

  title: string;

  logo: Maybe<string>;
};

export type ProjectMinifyFragmentFragment = {
  __typename?: "Project";

  id: string;

  title: string;

  description: string;

  skills: ProjectMinifyFragmentSkills[];
};

export type ProjectMinifyFragmentSkills = SkillIdTitleFragment;

export type ProjectFragmentFragment = {
  __typename?: "Project";

  image: string;

  work_experience: Maybe<ProjectFragmentWorkExperience>;
} & ProjectMinifyFragmentFragment;

export type ProjectFragmentWorkExperience = {
  __typename?: "WorkExperience";

  company: ProjectFragmentCompany;
};

export type ProjectFragmentCompany = CompanyFragmentFragment;

export type SkillIdTitleFragment = {
  __typename?: "Skill";

  id: string;

  title: string;
};

import * as ReactApollo from "react-apollo";
import * as React from "react";

import gql from "graphql-tag";

// ====================================================
// Fragments
// ====================================================

export const SkillIdTitleFragmentDoc = gql`
  fragment SkillIDTitle on Skill {
    id
    title
  }
`;

export const ProjectMinifyFragmentFragmentDoc = gql`
  fragment ProjectMinifyFragment on Project {
    id
    title
    description
    skills {
      ...SkillIDTitle
    }
  }

  ${SkillIdTitleFragmentDoc}
`;

export const CompanyFragmentFragmentDoc = gql`
  fragment CompanyFragment on Company {
    id
    title
    logo
  }
`;

export const ProjectFragmentFragmentDoc = gql`
  fragment ProjectFragment on Project {
    ...ProjectMinifyFragment
    image
    work_experience {
      company {
        ...CompanyFragment
      }
    }
  }

  ${ProjectMinifyFragmentFragmentDoc}
  ${CompanyFragmentFragmentDoc}
`;

// ====================================================
// Components
// ====================================================

export const ContactDocument = gql`
  mutation Contact($data: ContactInput!) {
    contact(data: $data)
  }
`;
export class ContactComponent extends React.Component<
  Partial<ReactApollo.MutationProps<ContactMutation, ContactVariables>>
> {
  render() {
    return (
      <ReactApollo.Mutation<ContactMutation, ContactVariables>
        mutation={ContactDocument}
        {...(this as any)["props"] as any}
      />
    );
  }
}
export type ContactProps<TChildProps = any> = Partial<
  ReactApollo.MutateProps<ContactMutation, ContactVariables>
> &
  TChildProps;
export type ContactMutationFn = ReactApollo.MutationFn<
  ContactMutation,
  ContactVariables
>;
export function ContactHOC<TProps, TChildProps = any>(
  operationOptions:
    | ReactApollo.OperationOption<
        TProps,
        ContactMutation,
        ContactVariables,
        ContactProps<TChildProps>
      >
    | undefined
) {
  return ReactApollo.graphql<
    TProps,
    ContactMutation,
    ContactVariables,
    ContactProps<TChildProps>
  >(ContactDocument, operationOptions);
}
export const GetAllProjectDocument = gql`
  query GetAllProject {
    getAllProject {
      ...ProjectFragment
    }
  }

  ${ProjectFragmentFragmentDoc}
`;
export class GetAllProjectComponent extends React.Component<
  Partial<ReactApollo.QueryProps<GetAllProjectQuery, GetAllProjectVariables>>
> {
  render() {
    return (
      <ReactApollo.Query<GetAllProjectQuery, GetAllProjectVariables>
        query={GetAllProjectDocument}
        {...(this as any)["props"] as any}
      />
    );
  }
}
export type GetAllProjectProps<TChildProps = any> = Partial<
  ReactApollo.DataProps<GetAllProjectQuery, GetAllProjectVariables>
> &
  TChildProps;
export function GetAllProjectHOC<TProps, TChildProps = any>(
  operationOptions:
    | ReactApollo.OperationOption<
        TProps,
        GetAllProjectQuery,
        GetAllProjectVariables,
        GetAllProjectProps<TChildProps>
      >
    | undefined
) {
  return ReactApollo.graphql<
    TProps,
    GetAllProjectQuery,
    GetAllProjectVariables,
    GetAllProjectProps<TChildProps>
  >(GetAllProjectDocument, operationOptions);
}
export const GetFeaturedProjectsDocument = gql`
  query GetFeaturedProjects {
    getFeaturedProjects {
      ...ProjectFragment
    }
  }

  ${ProjectFragmentFragmentDoc}
`;
export class GetFeaturedProjectsComponent extends React.Component<
  Partial<
    ReactApollo.QueryProps<
      GetFeaturedProjectsQuery,
      GetFeaturedProjectsVariables
    >
  >
> {
  render() {
    return (
      <ReactApollo.Query<GetFeaturedProjectsQuery, GetFeaturedProjectsVariables>
        query={GetFeaturedProjectsDocument}
        {...(this as any)["props"] as any}
      />
    );
  }
}
export type GetFeaturedProjectsProps<TChildProps = any> = Partial<
  ReactApollo.DataProps<GetFeaturedProjectsQuery, GetFeaturedProjectsVariables>
> &
  TChildProps;
export function GetFeaturedProjectsHOC<TProps, TChildProps = any>(
  operationOptions:
    | ReactApollo.OperationOption<
        TProps,
        GetFeaturedProjectsQuery,
        GetFeaturedProjectsVariables,
        GetFeaturedProjectsProps<TChildProps>
      >
    | undefined
) {
  return ReactApollo.graphql<
    TProps,
    GetFeaturedProjectsQuery,
    GetFeaturedProjectsVariables,
    GetFeaturedProjectsProps<TChildProps>
  >(GetFeaturedProjectsDocument, operationOptions);
}
export const GetProjectDocument = gql`
  query GetProject($id: String!) {
    getProject(id: $id) {
      ...ProjectFragment
      tagLine
      url
      completionTime
      sourceCode
      description_point
    }
  }

  ${ProjectFragmentFragmentDoc}
`;
export class GetProjectComponent extends React.Component<
  Partial<ReactApollo.QueryProps<GetProjectQuery, GetProjectVariables>>
> {
  render() {
    return (
      <ReactApollo.Query<GetProjectQuery, GetProjectVariables>
        query={GetProjectDocument}
        {...(this as any)["props"] as any}
      />
    );
  }
}
export type GetProjectProps<TChildProps = any> = Partial<
  ReactApollo.DataProps<GetProjectQuery, GetProjectVariables>
> &
  TChildProps;
export function GetProjectHOC<TProps, TChildProps = any>(
  operationOptions:
    | ReactApollo.OperationOption<
        TProps,
        GetProjectQuery,
        GetProjectVariables,
        GetProjectProps<TChildProps>
      >
    | undefined
) {
  return ReactApollo.graphql<
    TProps,
    GetProjectQuery,
    GetProjectVariables,
    GetProjectProps<TChildProps>
  >(GetProjectDocument, operationOptions);
}
export const GetCategorySkillsDocument = gql`
  query GetCategorySkills {
    getCategorySkills {
      ...SkillIDTitle
    }
  }

  ${SkillIdTitleFragmentDoc}
`;
export class GetCategorySkillsComponent extends React.Component<
  Partial<
    ReactApollo.QueryProps<GetCategorySkillsQuery, GetCategorySkillsVariables>
  >
> {
  render() {
    return (
      <ReactApollo.Query<GetCategorySkillsQuery, GetCategorySkillsVariables>
        query={GetCategorySkillsDocument}
        {...(this as any)["props"] as any}
      />
    );
  }
}
export type GetCategorySkillsProps<TChildProps = any> = Partial<
  ReactApollo.DataProps<GetCategorySkillsQuery, GetCategorySkillsVariables>
> &
  TChildProps;
export function GetCategorySkillsHOC<TProps, TChildProps = any>(
  operationOptions:
    | ReactApollo.OperationOption<
        TProps,
        GetCategorySkillsQuery,
        GetCategorySkillsVariables,
        GetCategorySkillsProps<TChildProps>
      >
    | undefined
) {
  return ReactApollo.graphql<
    TProps,
    GetCategorySkillsQuery,
    GetCategorySkillsVariables,
    GetCategorySkillsProps<TChildProps>
  >(GetCategorySkillsDocument, operationOptions);
}
export const GetMainSkillsDocument = gql`
  query GetMainSkills($isMain: Boolean) {
    getMainSkills(isMain: $isMain) {
      ...SkillIDTitle
    }
  }

  ${SkillIdTitleFragmentDoc}
`;
export class GetMainSkillsComponent extends React.Component<
  Partial<ReactApollo.QueryProps<GetMainSkillsQuery, GetMainSkillsVariables>>
> {
  render() {
    return (
      <ReactApollo.Query<GetMainSkillsQuery, GetMainSkillsVariables>
        query={GetMainSkillsDocument}
        {...(this as any)["props"] as any}
      />
    );
  }
}
export type GetMainSkillsProps<TChildProps = any> = Partial<
  ReactApollo.DataProps<GetMainSkillsQuery, GetMainSkillsVariables>
> &
  TChildProps;
export function GetMainSkillsHOC<TProps, TChildProps = any>(
  operationOptions:
    | ReactApollo.OperationOption<
        TProps,
        GetMainSkillsQuery,
        GetMainSkillsVariables,
        GetMainSkillsProps<TChildProps>
      >
    | undefined
) {
  return ReactApollo.graphql<
    TProps,
    GetMainSkillsQuery,
    GetMainSkillsVariables,
    GetMainSkillsProps<TChildProps>
  >(GetMainSkillsDocument, operationOptions);
}
export const GetAllStackDocument = gql`
  query GetAllStack($filters: StackSkillsFilterInput) {
    getAllStack {
      id
      title
      image
      skills(filters: $filters) {
        ...SkillIDTitle
        rating
      }
    }
  }

  ${SkillIdTitleFragmentDoc}
`;
export class GetAllStackComponent extends React.Component<
  Partial<ReactApollo.QueryProps<GetAllStackQuery, GetAllStackVariables>>
> {
  render() {
    return (
      <ReactApollo.Query<GetAllStackQuery, GetAllStackVariables>
        query={GetAllStackDocument}
        {...(this as any)["props"] as any}
      />
    );
  }
}
export type GetAllStackProps<TChildProps = any> = Partial<
  ReactApollo.DataProps<GetAllStackQuery, GetAllStackVariables>
> &
  TChildProps;
export function GetAllStackHOC<TProps, TChildProps = any>(
  operationOptions:
    | ReactApollo.OperationOption<
        TProps,
        GetAllStackQuery,
        GetAllStackVariables,
        GetAllStackProps<TChildProps>
      >
    | undefined
) {
  return ReactApollo.graphql<
    TProps,
    GetAllStackQuery,
    GetAllStackVariables,
    GetAllStackProps<TChildProps>
  >(GetAllStackDocument, operationOptions);
}
export const GetAllWorkExperienceDocument = gql`
  query GetAllWorkExperience {
    getAllWorkExperience {
      id
      title
      duration {
        start
        end
        present
      }
      company {
        ...CompanyFragment
      }
      description
      projects {
        ...ProjectMinifyFragment
        description_point
      }
      skills {
        ...SkillIDTitle
      }
      notes
    }
  }

  ${CompanyFragmentFragmentDoc}
  ${ProjectMinifyFragmentFragmentDoc}
  ${SkillIdTitleFragmentDoc}
`;
export class GetAllWorkExperienceComponent extends React.Component<
  Partial<
    ReactApollo.QueryProps<
      GetAllWorkExperienceQuery,
      GetAllWorkExperienceVariables
    >
  >
> {
  render() {
    return (
      <ReactApollo.Query<
        GetAllWorkExperienceQuery,
        GetAllWorkExperienceVariables
      >
        query={GetAllWorkExperienceDocument}
        {...(this as any)["props"] as any}
      />
    );
  }
}
export type GetAllWorkExperienceProps<TChildProps = any> = Partial<
  ReactApollo.DataProps<
    GetAllWorkExperienceQuery,
    GetAllWorkExperienceVariables
  >
> &
  TChildProps;
export function GetAllWorkExperienceHOC<TProps, TChildProps = any>(
  operationOptions:
    | ReactApollo.OperationOption<
        TProps,
        GetAllWorkExperienceQuery,
        GetAllWorkExperienceVariables,
        GetAllWorkExperienceProps<TChildProps>
      >
    | undefined
) {
  return ReactApollo.graphql<
    TProps,
    GetAllWorkExperienceQuery,
    GetAllWorkExperienceVariables,
    GetAllWorkExperienceProps<TChildProps>
  >(GetAllWorkExperienceDocument, operationOptions);
}
