import * as React from "react";
import Layout from "../components/Layout";
import HeaderIntroAbout from "../components/about/header-intro-about";
import SkillOverview from "../components/about/skill-overview";
import CategoryPanel from "../components/about/category-panel";
import FeatureProducts from "../components/about/feature-products";
import CTAFooter from "../components/cta-footer";
import { GetAllStackComponent } from "../generated/apolloComponent";
import Loader from "../components/Loader";

const AboutPage: React.FunctionComponent = () => (
  <Layout title="About | Next.js + TypeScript Example">
    <HeaderIntroAbout />
    <section className="skills-section section py-5">
      <div className="container">
        <SkillOverview />

        <div className="skills-blocks mx-auto pt-5">
          <div className="row">
            <GetAllStackComponent variables={{ filters: { isMain: true } }}>
              {({ data, loading }) => {
                if (loading) {
                  return <Loader className="col-12" />;
                }
                if (data && data.getAllStack && data.getAllStack.length > 0) {
                  return data.getAllStack.map(category => (
                    <CategoryPanel key={category.id} {...category} />
                  ));
                }
              }}
            </GetAllStackComponent>
          </div>
        </div>
      </div>
    </section>
    <FeatureProducts />
    <CTAFooter />
  </Layout>
);

export default AboutPage;
