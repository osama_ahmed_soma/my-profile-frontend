import React from "react";

import TopSection from "../components/resume/top-section";
import Header from "../components/resume/header";
import Summery from "../components/resume/summery";
import WorkExperience from "../components/resume/work-experience";
import Skills from "../components/resume/skills";
import Education from "../components/resume/education";
import Layout from "../components/Layout";

const Resume = () => {
  return (
    <Layout title="Resume">
      <TopSection />
      <article className="resume-wrapper text-center position-relative">
        <div className="resume-wrapper-inner mx-auto text-left bg-white shadow-lg">
          <Header />
          <div className="resume-body p-5">
            <Summery />
            <div className="row">
              <WorkExperience />
              <div className="col-lg-3">
                <Skills />
                <Education />
                <section className="resume-section language-section mb-5">
                  <h2 className="resume-section-title text-uppercase font-weight-bold pb-3 mb-3">
                    Language
                  </h2>
                  <div className="resume-section-content">
                    <ul className="list-unstyled resume-lang-list">
                      <li className="mb-2">
                        <span className="resume-lang-name font-weight-bold">
                          Urdu
                        </span>{" "}
                        <small className="text-muted font-weight-normal">
                          (Native)
                        </small>
                      </li>
                      <li className="mb-2">
                        <span className="resume-lang-name font-weight-bold">
                          English
                        </span>{" "}
                        <small className="text-muted font-weight-normal">
                          (Professional)
                        </small>
                      </li>
                      <li className="mb-2 align-middle">
                        <span className="resume-lang-name font-weight-bold">
                          Arabic
                        </span>{" "}
                        <small className="text-muted font-weight-normal">
                          (Beginner)
                        </small>
                      </li>
                    </ul>
                  </div>
                </section>
                <section className="resume-section interests-section mb-5">
                  <h2 className="resume-section-title text-uppercase font-weight-bold pb-3 mb-3">
                    Interests
                  </h2>
                  <div className="resume-section-content">
                    <ul className="list-unstyled">
                      <li className="mb-1">Gaming</li>
                      <li className="mb-1">Music</li>
                      <li className="mb-1">Sports</li>
                      <li className="mb-1">Coding Challenges</li>
                    </ul>
                  </div>
                </section>
              </div>
            </div>
          </div>
        </div>
      </article>
    </Layout>
  );
};

export default Resume;
