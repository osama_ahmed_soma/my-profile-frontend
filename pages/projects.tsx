import React from "react";
import CTAFooter from "../components/cta-footer";
import ProjectPanel from "../components/project-panel";
import Layout from "../components/Layout";
import {
  GetCategorySkillsHOC,
  GetAllProjectHOC,
  GetAllProjectProps,
  GetCategorySkillsProps,
  ProjectFragmentFragment
} from "../generated/apolloComponent";
import { compose } from "react-apollo";
import Loader from "../components/Loader";

interface State {
  activeID: string;
  skills: any[];
  projects: any[];
  isLoading: boolean;
}

interface Props {
  getAllProjects: GetAllProjectProps;
  getCategorySkills: GetCategorySkillsProps;
}

class Projects extends React.Component<Props, State> {
  state: Readonly<State> = {
    activeID: "all",
    skills: [],
    projects: [],
    isLoading: false
  };

  setLoading = (isLoading: boolean = true) => {
    if (this.state.isLoading !== isLoading) {
      this.setState(() => {
        isLoading;
      });
    }
  };

  setSkills = () => {
    const { getCategorySkills, loading } = this.props.getCategorySkills;
    if (loading) {
      this.setLoading();
    } else {
      const skills = getCategorySkills;
      if (skills) {
        if (!!!skills.find((skill: any) => skill.id === "all")) {
          skills.unshift({
            id: "all",
            title: "All"
          });
        }
        this.setState(() => ({ skills }));
        this.setLoading(false);
        this.setProjects();
      }
    }
  };

  componentDidMount() {
    this.setSkills();
  }

  componentDidUpdate() {
    if (this.state.skills.length <= 0) {
      this.setSkills();
    }
    if (this.state.projects.length <= 0) {
      this.setProjects();
    }
  }

  setProjects = (): void => {
    const { getAllProject, loading } = this.props.getAllProjects;
    if (loading) {
      this.setLoading();
    } else {
      let projects: ProjectFragmentFragment[] = getAllProject;
      if (this.state.activeID !== "all") {
        projects = projects.filter(
          ({ skills }) => !!skills.find(({ id }) => id === this.state.activeID)
        );
      }
      this.setState(() => ({ projects }));
    }
  };

  activeMe = (skillID: string): void =>
    this.setState(() => ({ activeID: skillID }), () => this.setProjects());
  render() {
    return (
      <Layout title="Projects">
        <div className="header-intro theme-bg-primary text-white py-5">
          <div className="container">
            <h2 className="page-heading mb-3">Projects</h2>
            <div className="page-heading-tagline">
              In-depth Case Studies to show you what I can offer and how I work
            </div>
          </div>
        </div>
        <section className="section pt-5">
          <div className="container">
            {this.state.isLoading && <Loader />}
            {!this.state.isLoading && (
              <>
                <div className="text-center">
                  <ul id="filters" className="filters mb-5 mx-auto pl-0">
                    {this.state.skills.map(({ id, title }) => (
                      <li
                        key={id}
                        onClick={() => this.activeMe(id)}
                        className={`type ${
                          this.state.activeID === id ? "active" : ""
                        }`}
                      >
                        {title}
                      </li>
                    ))}
                  </ul>
                </div>
                <div className="project-cards row mb-5">
                  {this.state.projects.map(project => (
                    <ProjectPanel key={project.id} {...project} />
                  ))}
                </div>
              </>
            )}
          </div>
        </section>
        <CTAFooter />
      </Layout>
    );
  }
}

export default compose(
  GetAllProjectHOC({ name: "getAllProjects" }),
  GetCategorySkillsHOC({ name: "getCategorySkills" })
)(Projects);
