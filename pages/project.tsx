import * as React from "react";
import { withRouter, WithRouterProps } from "next/router";
import Layout from "../components/Layout";
import TechnologiesPanel from "../components/technologies-panel";
import { GetProjectComponent, Maybe } from "../generated/apolloComponent";
import Loader from "../components/Loader";

interface TopSectionProps {
  name: string;
  tagLine: Maybe<string>;
  image: Maybe<string>;
}

const TopSection: React.SFC<TopSectionProps> = ({ name, tagLine, image }) => (
  <div className="header-intro theme-bg-primary text-white py-5">
    <div className="container position-relative ">
      <h2 className="page-heading mb-3">{name}</h2>
      {tagLine && <div className="page-heading-tagline">{tagLine}</div>}
      {image && (
        <img
          src={image}
          alt=""
          className="download-resume position-absolute font-weight-bold mx-auto"
          style={{
            width: "250px",
            backgroundColor: "#4980bd",
            padding: "10px",
            borderRadius: "10px"
          }}
        />
      )}
    </div>
  </div>
);

const Project: React.SFC<WithRouterProps> = ({ router }) => {
  let projectID = "";
  if (router && router.query) {
    const {
      query: { id }
    } = router;
    if (id) {
      projectID = id as string;
    }
  }
  return (
    <Layout title="Project">
      <GetProjectComponent variables={{ id: projectID }}>
        {({ data, loading }) => {
          if (loading) {
            return <Loader />;
          }
          if (data && data.getProject && data.getProject.work_experience) {
            const {
              title,
              tagLine,
              work_experience: {
                company: { logo }
              },
              image,
              description,
              skills,
              url,
              completionTime,
              sourceCode,
              description_point
            } = data.getProject;
            const topSectionData: TopSectionProps = {
              name: title,
              tagLine,
              image: logo
            };
            return (
              <>
                <TopSection {...topSectionData} />
                <section className="section pt-5">
                  <div className="container w-60">
                    {image && (
                      <img src={image} alt="" className="w-100 d-block" />
                    )}
                    <h4 className="resume-section-title no-border text-uppercase font-weight-bold mb-4 mt-4">
                      Project Background
                    </h4>
                    <div className="resume-section-content">
                      <p className="mb-3">{description}</p>
                      {description_point && description_point.length > 0 && (
                        <ol>
                          {description_point.map((dp, i) => (
                            <li key={i}>{dp}</li>
                          ))}
                        </ol>
                      )}
                    </div>
                    {skills && skills.length > 0 && (
                      <>
                        <h4 className="resume-section-title no-border text-uppercase font-weight-bold mb-4 mt-4">
                          Technologies Used
                        </h4>
                        <div className="resume-section-content">
                          <ul className="list-inline mb-0 mx-auto mb-3">
                            {skills.map(skill => (
                              <TechnologiesPanel key={skill.id} {...skill} />
                            ))}
                          </ul>
                        </div>
                      </>
                    )}
                    {(url || completionTime || sourceCode) && (
                      <>
                        <h4 className="resume-section-title no-border text-uppercase font-weight-bold mb-4 mt-4">
                          Other Detail
                        </h4>
                        <div className="resume-section-content">
                          {url && (
                            <p className="mb-3">
                              URL: <b>{url}</b>
                            </p>
                          )}
                          {completionTime && (
                            <p className="mb-3">
                              Complete In: <b>{completionTime}</b>
                            </p>
                          )}
                          {sourceCode && (
                            <p className="mb-3">
                              Source Code: <b>{sourceCode}</b>
                            </p>
                          )}
                        </div>
                      </>
                    )}
                  </div>
                </section>
              </>
            );
          }
        }}
      </GetProjectComponent>
    </Layout>
  );
};

export default withRouter(Project);
