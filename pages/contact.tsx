import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import ContactForm from "../components/contact-form";
import MyLink from "../components/MyLink";
import Layout from "../components/Layout";

const Contact = () => {
  return (
    <Layout title="Contact Me">
      <div className="header-intro theme-bg-primary text-white py-5">
        <div className="container">
          <h2 className="page-heading mb-0">Contact</h2>
        </div>
      </div>
      <section className="section py-5">
        <div className="container">
          <div className="row">
            <div className="contact-intro col-lg-8 mx-lg-auto mb-5 text-center">
              <img
                className="profile-small d-inline-block mx-auto rounded-circle mb-3"
                src={require("../assets/images/profile.jpg")}
                alt=""
              />

              <div className="speech-bubble bg-white p-4 p-lg-5 shadow-sm">
                <p className="text-left mb-3">
                  I'm currently taking on freelance work. If you are interested
                  in hiring me for your project please use the form below to get
                  in touch. Want to know how I work and what I can offer? Check
                  out my <MyLink href="/projects">project case studies</MyLink>{" "}
                  and <MyLink href="/resume">resume</MyLink>.
                </p>
                <h6 className="font-weight-bold text-center mb-3">
                  You can also find me on the following channels
                </h6>

                <ul className="social-list-color list-inline mb-0">
                  <li className="list-inline-item mb-3">
                    <a
                      className="linkedin"
                      href="https://www.linkedin.com/in/osama-ahmed-705026130/"
                      target="_blank"
                    >
                      <FontAwesomeIcon icon={["fab", "linkedin-in"]} />
                    </a>
                  </li>
                  <li className="list-inline-item mb-3">
                    <a
                      className="github"
                      href="https://bitbucket.org/osama_ahmed_soma/"
                      target="_blank"
                    >
                      <FontAwesomeIcon icon={["fab", "github-alt"]} />
                    </a>
                  </li>
                  <li className="list-inline-item mb-3">
                    <a
                      className="skype"
                      href="skype:soma-hbb?call"
                      target="_top"
                    >
                      <FontAwesomeIcon icon={["fab", "skype"]} />
                    </a>
                  </li>
                </ul>
              </div>

              <div className="speech-bubble bg-white p-4 p-lg-5 shadow-sm mt-5">
                <h6>Phone Number: +92 332 0269679</h6>
                <h6>Skype: soma-hbb</h6>
                <h6>Email: osama.ahmed220@gmail.com</h6>
              </div>
            </div>
            <ContactForm />
          </div>
        </div>
      </section>
    </Layout>
  );
};

export default Contact;
