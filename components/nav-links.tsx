import React, { Component, Fragment } from "react";
import Nav from "./nav";

interface SlideLineData {
  width: string;
  left: string;
}

interface State {
  slideLineData: SlideLineData;
}

export interface NavData {
  title: string;
  path: string;
  active: boolean;
}

class NavLinks extends Component<any, State> {
  state: Readonly<State> = {
    slideLineData: {
      width: "",
      left: ""
    }
  };

  navLinks: NavData[] = [
    {
      title: "About",
      path: "/about",
      active: true
    },
    {
      title: "Projects",
      path: "/projects",
      active: false
    },
    {
      title: "Resume",
      path: "/resume",
      active: false
    },
    {
      title: "Contact",
      path: "/contact",
      active: false
    }
  ];

  componentDidMount() {
    window.onresize = () => {
      this.triggerNavEvent();
    };
    this.triggerNavEvent();
  }

  triggerNavEvent = (): void =>
    this.navHoverEffect({
      target: document.querySelector(".nav-item .nav-link.active")
    });

  getOffset = (el: any): { top: number; left: number } => {
    const rect = el.getBoundingClientRect();
    const scrollLeft =
      window.pageXOffset || document.documentElement.scrollLeft;
    const scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    return { top: rect.top + scrollTop, left: rect.left + scrollLeft };
  };

  navHoverEffect = (event: any): void => {
    if (event.target) {
      this.setState({
        slideLineData: {
          width: `${event.target.offsetWidth}px`,
          left: `${this.getOffset(event.target).left - 15}px`
        }
      });
    }
  };

  render() {
    return (
      <Fragment>
        <ul className="navbar-nav ml-lg-auto">
          {this.navLinks.map((item, index) => (
            <Nav key={index} data={item} onMouseOver={this.navHoverEffect} />
          ))}
        </ul>
        <span
          id="slide-line"
          style={{
            width:
              this.state && this.state.slideLineData
                ? this.state.slideLineData.width
                : "",
            left:
              this.state && this.state.slideLineData
                ? this.state.slideLineData.left
                : ""
          }}
        />
        {/* </div> */}
      </Fragment>
    );
  }
}

export default NavLinks;
