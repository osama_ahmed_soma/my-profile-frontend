import * as React from "react";
import Link from "next/link";
import Head from "next/head";
import ScrollToTop from "./scroll-to-top";
import Header from "./header";
import Footer from "./footer";

type Props = {
  title?: string;
};

const Layout: React.FunctionComponent<Props> = ({
  children,
  title = "This is the default title"
}) => (
  <div>
    <Head>
      <title>{title}</title>
      <meta charSet="utf-8" />
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      <link
        href="//fonts.googleapis.com/css?family=Roboto:400,500,400italic,300italic,300,500italic,700,700italic,900,900italic"
        rel="stylesheet"
        type="text/css"
      />
    </Head>
    <Header />
    {children}
    <Footer />
    <ScrollToTop scrollStepInPx={50} delayInMs={16.66} />
  </div>
);

export default Layout;
