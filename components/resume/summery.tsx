import * as React from "react";

const Summery = () => (
  <section className="resume-section summary-section mb-5">
    <h2 className="resume-section-title text-uppercase font-weight-bold pb-3 mb-3">
      Career Summary
    </h2>
    <div className="resume-section-content">
      <p className="mb-0">
        Senior Web and cross platform Mobile Application and Software Developer
        specializing in FrontEnd, BackEnd Security, State Management, Real Time
        Data Transfer and Business Logic development.
        <br /> Experienced with all stages of the development cycle for dynamic
        web, cross platform mobile and software projects. Well-versed in
        numerous programming languages including JavaScript, React.js, Angular,
        Redux, State Management, Sockets, Node.js, Dart (Flutter), NoSQL, MySQL,
        PHP, Python and TypeScript. Strong background in project management and
        customer relations.
      </p>
    </div>
  </section>
);

export default Summery;
