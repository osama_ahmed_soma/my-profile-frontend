import React from "react";

const Education = () => (
  <section className="resume-section education-section mb-5">
    <h2 className="resume-section-title text-uppercase font-weight-bold pb-3 mb-3">
      Education
    </h2>
    <div className="resume-section-content">
      <ul className="list-unstyled">
        <li className="mb-2">
          <div className="resume-degree font-weight-bold">
            Intermediate in Pre Engineering
          </div>
          <div className="resume-degree-org">Govt. Degree Boys College 5-L</div>
          <div className="resume-degree-time">2010</div>
        </li>
      </ul>
    </div>
  </section>
);

export default Education;
