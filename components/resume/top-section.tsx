import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const TopSection = () => {
  return (
    <React.Fragment>
      <div className="header-intro header-intro-resume theme-bg-primary text-white py-5">
        <div className="container position-relative">
          <h2 className="page-heading mb-3">Resume</h2>
          <a
            className="btn theme-btn-on-bg download-resume position-absolute font-weight-bold mx-auto"
            href="https://drive.google.com/uc?export=download&id=1O8puSDfq_idTrndmxnS3eX-7aOcSXbI5"
            target="_top"
            download
          >
            <FontAwesomeIcon icon="download" /> Download PDF Version
          </a>
        </div>
      </div>
    </React.Fragment>
  );
};

export default TopSection;
