import React from "react";
import "./work-experience.css";
import TechnologiesPanel from "../technologies-panel";
import {
  GetAllWorkExperienceComponent,
  GetAllWorkExperienceGetAllWorkExperience,
  GetAllWorkExperienceProjects,
  SkillIdTitleFragment
} from "../../generated/apolloComponent";
import Loader from "../Loader";

const TechnologiesSectionWrapper: React.SFC<{
  techs: SkillIdTitleFragment[];
}> = ({ techs }) => (
  <React.Fragment>
    <h4 className="resume-timeline-item-desc-heading font-weight-bold">
      Technologies used:
    </h4>
    <ul className="list-inline">
      {techs &&
        techs.map(skill => <TechnologiesPanel key={skill.id} {...skill} />)}
    </ul>
  </React.Fragment>
);

const ProjectSection: React.SFC<GetAllWorkExperienceProjects> = ({
  title,
  description,
  skills,
  description_point
}) => (
  <React.Fragment>
    <h4 className="resume-timeline-item-desc-heading font-weight-bold">
      {title}
    </h4>
    <div className="project-detail">
      <p>{description}</p>
      {description_point && description_point.length > 0 && (
        <ol>
          {description_point.map((d: any, i: number) => (
            <li key={i}>{d}</li>
          ))}
        </ol>
      )}
      {skills.length > 0 && <TechnologiesSectionWrapper techs={skills} />}
    </div>
  </React.Fragment>
);

const months: string[] = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December"
];

const WorkSection: React.SFC<GetAllWorkExperienceGetAllWorkExperience> = ({
  title,
  company: { title: companyTitle },
  duration: { start, end, present },
  description,
  projects,
  notes,
  skills
}) => {
  return (
    <article className="resume-timeline-item position-relative pb-5">
      <div className="resume-timeline-item-header mb-2">
        <div className="d-flex flex-column flex-md-row">
          <h3 className="resume-position-title font-weight-bold mb-1">
            {title}
          </h3>
          <div className="resume-company-name ml-auto">{companyTitle}</div>
        </div>
        <div className="resume-position-time">
          {months[new Date(start).getMonth()] +
            " " +
            new Date(start).getFullYear()}{" "}
          -{" "}
          {present
            ? "Present"
            : new Date(end).getMonth() + " " + new Date(end).getFullYear()}
        </div>
      </div>
      <div className="resume-timeline-item-desc">
        <p>{description}</p>
        {projects && <h4>Projects:</h4>}
        {projects &&
          projects.map(project => (
            <React.Fragment key={project.id}>
              <ProjectSection {...project} />
            </React.Fragment>
          ))}
        {notes && (
          <p>
            <b>Note:</b> {notes}
          </p>
        )}
        {skills.length > 0 && <TechnologiesSectionWrapper techs={skills} />}
      </div>
    </article>
  );
};

const WorkExperience = () => {
  return (
    <div className="col-lg-9">
      <section className="resume-section experience-section mb-5">
        <h2 className="resume-section-title text-uppercase font-weight-bold pb-3 mb-3">
          Work Experience
        </h2>
        <div className="resume-section-content">
          <div className="resume-timeline position-relative">
            <GetAllWorkExperienceComponent>
              {({ data, loading }) => {
                if (loading) {
                  return <Loader />;
                }
                if (
                  data &&
                  data.getAllWorkExperience &&
                  data.getAllWorkExperience.length > 0
                ) {
                  return data.getAllWorkExperience
                    .sort(
                      (f, l) =>
                        new Date(l.duration.start).getFullYear() -
                        new Date(f.duration.start).getFullYear()
                    )
                    .map(exp => (
                      <React.Fragment key={exp.id}>
                        <WorkSection {...exp} />
                      </React.Fragment>
                    ));
                }
              }}
            </GetAllWorkExperienceComponent>
          </div>
        </div>
      </section>
    </div>
  );
};

export default WorkExperience;
