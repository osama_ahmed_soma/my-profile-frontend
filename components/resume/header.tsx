import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const Header = () => (
  <header className="resume-header pt-4 pt-md-0">
    <div className="media flex-column flex-md-row">
      <img
        className="mr-3 img-fluid picture mx-auto"
        src={require("../../assets/images/profile.jpg")}
        alt=""
      />
      <div className="media-body p-4 d-flex flex-column flex-md-row mx-auto mx-lg-0">
        <div className="primary-info">
          <h1 className="name mt-0 mb-1 text-white text-uppercase text-uppercase">
            Osama Ahmed
          </h1>
          <div className="title mb-3">Full Stack Developer</div>
          <ul className="list-unstyled">
            <li className="mb-2">
              <a href="mailto:osama.ahmed220@gmail.com" target="_top">
                <FontAwesomeIcon
                  icon={["far", "envelope"]}
                  className="fa-fw mr-2"
                  transform="grow-3"
                />{" "}
                osama.ahmed220@gmail.com
              </a>
            </li>
            <li>
              <a href="tel:+923320269679">
                <FontAwesomeIcon
                  icon="mobile-alt"
                  className="fa-fw mr-2"
                  transform="grow-6"
                />{" "}
                +92 332 0269679
              </a>
            </li>
          </ul>
        </div>
        <div className="secondary-info ml-md-auto mt-2">
          <ul className="resume-social list-unstyled">
            <li className="mb-3">
              <a
                href="https://www.linkedin.com/in/osama-ahmed-705026130/"
                target="_blank"
              >
                <span className="fa-container text-center mr-2">
                  <FontAwesomeIcon icon={["fab", "linkedin-in"]} />
                </span>
                osama-ahmed-705026130
              </a>
            </li>
            <li className="mb-3">
              <a href="https://bitbucket.org/osama_ahmed_soma/" target="_blank">
                <span className="fa-container text-center mr-2">
                  <FontAwesomeIcon icon={["fab", "github-alt"]} />
                </span>
                osama_ahmed_soma
              </a>
            </li>
            <li className="mb-3">
              <a href="skype:soma-hbb?call" target="_top">
                <span className="fa-container text-center mr-2">
                  <FontAwesomeIcon icon={["fab", "skype"]} />
                </span>
                soma-hbb
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </header>
);

export default Header;
