import React from "react";
import {
  GetAllStackComponent,
  GetMainSkillsComponent
} from "../../generated/apolloComponent";
import TechnologiesPanel from "../technologies-panel";
import Loader from "../Loader";

const Skills = () => {
  return (
    <section className="resume-section skills-section mb-5">
      <h2 className="resume-section-title text-uppercase font-weight-bold pb-3 mb-3">
        Skills &amp; Tools
      </h2>
      <div className="resume-section-content">
        <GetAllStackComponent>
          {({ data, loading }) => {
            if (loading) {
              return <Loader />;
            }
            if (data && data.getAllStack && data.getAllStack.length > 0) {
              return data.getAllStack.map(({ id, title, skills }) => (
                <div key={id} className="resume-skill-item">
                  <h4 className="resume-skills-cat font-weight-bold">
                    {title}
                  </h4>
                  {skills && (
                    <ul className="list-unstyled mb-4">
                      {skills
                        .sort((sf, sl) => sl.rating - sf.rating)
                        .map(({ rating, ...skill }) => (
                          <li key={skill.id} className="mb-2">
                            <div className="resume-skill-name">
                              {skill.title}
                            </div>
                            <div className="progress resume-progress">
                              <div
                                className="progress-bar theme-progress-bar-dark"
                                role="progressbar"
                                style={{ width: `${rating}%` }}
                              />
                            </div>
                          </li>
                        ))}
                    </ul>
                  )}
                </div>
              ));
            }
          }}
        </GetAllStackComponent>

        <div className="resume-skill-item">
          <h4 className="resume-skills-cat font-weight-bold">Others</h4>
          <GetMainSkillsComponent variables={{ isMain: false }}>
            {({ data, loading }) => {
              if (loading) {
                return <Loader />;
              }
              if (data && data.getMainSkills && data.getMainSkills.length > 0) {
                return (
                  <ul className="list-inline">
                    {data.getMainSkills.map(skill => (
                      <TechnologiesPanel
                        key={skill.id}
                        {...skill}
                        isLight={true}
                      />
                    ))}
                  </ul>
                );
              }
            }}
          </GetMainSkillsComponent>
        </div>
      </div>
    </section>
  );
};

export default Skills;
