import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

interface State {
  intervalId: NodeJS.Timeout;
  opacity: number;
}

interface Props {
  scrollStepInPx?: number;
  delayInMs?: number;
}

class ScrollToTop extends Component<Props, State> {
  static defaulProps: Props = {
    scrollStepInPx: 50,
    delayInMs: 16.66
  };

  state: Readonly<State> = {
    intervalId: null,
    opacity: 0
  };

  componentDidMount() {
    window.addEventListener("scroll", this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }

  handleScroll = () => {
    const lastScrollY = window.scrollY;
    if (lastScrollY === 0) {
      // set opacity
      this.setState(() => ({ opacity: 0 }));
    } else {
      this.setState(() => ({ opacity: 1 }));
    }
  };

  scrollStep = () => {
    if (window.pageYOffset === 0) {
      clearInterval(this.state.intervalId);
    }
    if (this.props.scrollStepInPx) {
      window.scroll(0, window.pageYOffset - this.props.scrollStepInPx);
    }
  };

  scrollToTop = () => {
    const intervalId = setInterval(this.scrollStep, this.props.delayInMs);
    this.setState({ intervalId });
  };

  render() {
    return (
      <div
        id="topcontrol"
        title="Scroll Back to Top"
        style={{
          position: "fixed",
          bottom: "5px",
          right: "5px",
          opacity: this.state.opacity,
          cursor: "pointer"
        }}
        onClick={this.scrollToTop}
      >
        <FontAwesomeIcon icon="angle-up" />
      </div>
    );
  }
}

export default ScrollToTop;
