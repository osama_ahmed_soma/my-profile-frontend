import React from "react";
import Link from "next/link";

interface Props {
  className?: string;
  href: string;
  onClick?: () => void;
}

const MyLink: React.SFC<Props> = ({ href, className, children, onClick }) => {
  return (
    <>
      <Link href={href}>
        <a className={className} onClick={onClick}>
          {children}
        </a>
      </Link>
    </>
  );
};

export default MyLink;
