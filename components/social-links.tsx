import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const SocialLinks = () => {
  return (
    <React.Fragment>
      <ul className="social-list list-inline mb-0">
        <li className="list-inline-item">
          <a
            className="text-white"
            href="https://www.linkedin.com/in/osama-ahmed-705026130/"
            target="_blank"
          >
            <FontAwesomeIcon icon={["fab", "linkedin-in"]} />
          </a>
        </li>
        <li className="list-inline-item">
          <a
            className="text-white"
            href="https://bitbucket.org/osama_ahmed_soma/"
            target="_blank"
          >
            <FontAwesomeIcon icon={["fab", "github-alt"]} />
          </a>
        </li>
        <li className="list-inline-item">
          <a className="text-white" href="skype:soma-hbb?call" target="_top">
            <FontAwesomeIcon icon={["fab", "skype"]} />
          </a>
        </li>
      </ul>
    </React.Fragment>
  );
};

export default SocialLinks;
