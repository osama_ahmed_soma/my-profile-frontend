import React, { Component } from "react";
import { Container, Navbar } from "reactstrap";
import SocialLinks from "./social-links";
// import HeaderIntro from "./headerIntro";
import NavLinks from "./nav-links";
import NavbarToggler from "reactstrap/lib/NavbarToggler";
import Collapse from "reactstrap/lib/Collapse";

interface State {
  collapsed: boolean;
}

class Header extends Component<{}, State> {
  state: Readonly<State> = {
    collapsed: true
  };
  toggleNavbar = () => {
    this.setState({
      collapsed: !this.state.collapsed
    });
  };
  render() {
    return (
      <header className="header">
        <div className="top-bar theme-bg-primary-darken">
          <Container fluid={true}>
            <Navbar className="navbar navbar-expand-lg navbar-dark position-relative">
              <SocialLinks />

              <NavbarToggler onClick={this.toggleNavbar} className="mr-2" />
              <Collapse isOpen={!this.state.collapsed} navbar>
                <NavLinks />
              </Collapse>
            </Navbar>
          </Container>
        </div>
        {/* <HeaderIntro /> */}
      </header>
    );
  }
}

export default Header;
