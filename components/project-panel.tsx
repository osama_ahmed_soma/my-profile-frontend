import React from "react";
import TechnologiesPanel from "./technologies-panel";
import MyLink from "./MyLink";
import { GetFeaturedProjectsGetFeaturedProjects } from "../generated/apolloComponent";
const ProjectPanel: React.SFC<GetFeaturedProjectsGetFeaturedProjects> = ({
  id,
  title,
  description,
  image,
  skills,
  work_experience
}) => {
  const companyName: string =
    work_experience && work_experience.company && work_experience.company.title
      ? work_experience.company.title
      : "";
  return (
    <div className="col-12 col-lg-4">
      <div className="card rounded-0 border-0 shadow-sm mb-5 mb-lg-0">
        <div className="card-img-container position-relative">
          <img className="card-img-top rounded-0" src={image} alt="" />

          <div className="card-img-overlay overlay-logo text-center">
            <div className="project-logo pl-3 pr-3">
              <h3
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  padding: 0,
                  margin: 0,
                  minHeight: "55.09px"
                }}
              >
                {companyName}
              </h3>
            </div>
          </div>
          <MyLink
            className="card-img-overlay overlay-content text-left p-lg-4"
            href={`/project/${id}`}
          >
            <h5 className="card-title font-weight-bold">
              Client: {companyName}
            </h5>
            <p className="card-text">{description}</p>
          </MyLink>
        </div>
        <div className="card-body pb-0">
          <h4 className="card-title text-truncate text-center mb-0">
            <MyLink href={`/project/${id}`}>{title}</MyLink>
          </h4>
        </div>

        <div className="card-footer border-0 text-center bg-white pb-4">
          <ul className="list-inline mb-0 mx-auto">
            {skills &&
              skills.map(skill => (
                <TechnologiesPanel key={skill.id} {...skill} />
              ))}
          </ul>
        </div>
      </div>
    </div>
  );
};

export default ProjectPanel;
