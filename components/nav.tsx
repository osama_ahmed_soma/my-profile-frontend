import React from "react";
import { NavData } from "./nav-links";
import MyLink from "./MyLink";
import { withRouter, WithRouterProps } from "next/router";

interface Props {
  data: NavData;
  onMouseOver: (el: any) => void;
}

const Nav: React.SFC<Props & WithRouterProps> = ({
  onMouseOver,
  data: { title, path },
  router
}) => {
  let className = "nav-link";
  if (router) {
    if (router.pathname === path) {
      className = className + " active";
    }
  }

  return (
    <li className="nav-item mr-lg-3" onMouseOver={onMouseOver}>
      <MyLink className={className} href={path}>
        {title}
      </MyLink>
    </li>
  );
};

export default withRouter(Nav);
