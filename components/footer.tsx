import React from "react";

const Footer = () => {
  return (
    <footer className="footer text-light text-center py-2">
      <small className="copyright">
        © {new Date().getFullYear()} Osama Ahmed Profile
      </small>
    </footer>
  );
};

export default Footer;
