import React from "react";
import MyLink from "../MyLink";

const HeaderIntroAbout = () => {
  return (
    <div className="header-intro theme-bg-primary text-white py-5">
      <div className="container">
        <div className="profile-teaser media flex-column flex-md-row">
          <img
            className="profile-image mb-3 mb-md-0 mr-md-5 ml-md-0 rounded mx-auto"
            src={require("../../assets/images/profile.jpg")}
            alt=""
          />
          <div className="media-body text-center text-md-left">
            <div className="lead">Hello, my name is</div>
            <h2 className="mt-0 display-4 font-weight-bold">Osama Ahmed</h2>
            <div className="bio mb-3">
              I'm a Senior Web and cross platform Mobile Application and
              Software Developer specializing in FrontEnd, BackEnd Security,
              State Management, Real Time Data Transfer and Business Logic
              development.
              <div>
                Experienced with all stages of the development cycle for dynamic
                web, cross platform mobile and software projects. Well-versed in
                numerous programming languages.
                <div>
                  Check out my project{" "}
                  <MyLink className="link-on-bg" href="/projects">
                    case studies
                  </MyLink>{" "}
                  and{" "}
                  <MyLink className="link-on-bg" href="/resume">
                    resume
                  </MyLink>
                  .
                </div>
              </div>
            </div>
            <MyLink
              className="theme-btn-on-bg btn font-weight-bold theme-btn-cta"
              href="/contact"
            >
              Hire Me
            </MyLink>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HeaderIntroAbout;
