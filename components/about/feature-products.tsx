import React from "react";
import ProjectPanel from "../project-panel";
import MyLink from "../MyLink";
import { GetFeaturedProjectsComponent } from "../../generated/apolloComponent";
import Loader from "../Loader";

const FeatureProducts = () => {
  return (
    <section className="section-featured-projects py-5">
      <div className="container">
        <h3 className="section-title font-weight-bold text-center mb-5">
          Featured Projects
        </h3>

        <div className="project-cards row mb-5">
          <GetFeaturedProjectsComponent>
            {({ data, loading }) => {
              if (loading) {
                return <Loader className="col-12" />;
              }
              if (
                data &&
                data.getFeaturedProjects &&
                data.getFeaturedProjects.length > 0
              ) {
                return data.getFeaturedProjects.map(project => (
                  <ProjectPanel key={project.id} {...project} />
                ));
              }
            }}
          </GetFeaturedProjectsComponent>
        </div>
        <div className="text-center">
          <MyLink className="btn btn-primary" href="/projects">
            View all projects
          </MyLink>
        </div>
      </div>
    </section>
  );
};

export default FeatureProducts;
