import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { GetAllStackSkills } from "../../generated/apolloComponent";

const SkillSection: React.SFC<GetAllStackSkills> = ({ title }) => {
  return (
    <li className="mb-2">
      <FontAwesomeIcon icon="check" className="mr-2 text-primary" />
      {title}
    </li>
  );
};

export default SkillSection;
