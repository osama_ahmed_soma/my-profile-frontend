import * as React from "react";
import MyLink from "../MyLink";

const SkillOverview = () => {
  return (
    <div>
      <h3 className="section-title font-weight-bold text-center mb-3">
        Skills Overview
      </h3>
      <div className="section-intro mx-auto text-center mb-5 text-secondary">
        I have more than 9 years' experience building rich web and mobile
        applications for clients all over the world. Below is a quick overview
        of my main technical skill sets and tools I use. Want to find out more
        about my experience?{" "}
        <MyLink href="/resume">Check out my online resume</MyLink>.
      </div>
    </div>
  );
};

export default SkillOverview;
