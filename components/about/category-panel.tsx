import React from "react";
import SkillSection from "./skill-section";
import { GetAllStackGetAllStack } from "../../generated/apolloComponent";

const CategoryPanel: React.SFC<GetAllStackGetAllStack> = ({
  title,
  image,
  skills
}) => {
  if (!image) {
    image = require("../../assets/images/other-skills-icon.svg");
  }
  return (
    <div className="skills-block col-12 col-lg-4 mb-5 mb-3 mb-lg-0">
      <div className="skills-block-inner bg-white shadow-sm py-4 px-5 position-relative">
        <h4 className="skills-cat text-center mb-3 mt-5">{title}</h4>
        <div className="skills-icon-holder position-absolute d-inline-block rounded-circle text-center">
          {image && <img className="skills-icon" src={image} />}
        </div>
        <ul className="skills-list list-unstyled text-secondary">
          {skills &&
            skills.map(skill => <SkillSection key={skill.id} {...skill} />)}
        </ul>
      </div>
    </div>
  );
};

export default CategoryPanel;
