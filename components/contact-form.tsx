import React from "react";
import { Form, Field, Formik } from "formik";
import { validateContactSchema } from "../shared/contact-form-validation-schema";
import InputField from "./input-field";
import { ContactComponent } from "../generated/apolloComponent";

interface FormValues {
  name: string;
  email: string;
  message: string;
}

const ContactForm = () => {
  return (
    <ContactComponent>
      {mutate => {
        return (
          <Formik<FormValues>
            initialValues={{
              name: "",
              email: "",
              message: ""
            }}
            onSubmit={async (values, { setSubmitting, resetForm }) => {
              setSubmitting(true);
              const response = await mutate({
                variables: {
                  data: values
                }
              });
              if (response && response.data && response.data.contact) {
                resetForm();
                setSubmitting(false);
                const successMessage = document.querySelector(
                  ".successMessage"
                );
                if (successMessage) {
                  (successMessage as any).style.opacity = "1";
                  setTimeout(
                    () => ((successMessage as any).style.opacity = "0"),
                    10000
                  );
                }
              }
            }}
            validationSchema={validateContactSchema}
            render={({ isSubmitting }) => (
              <Form className="contact-form col-lg-8 mx-lg-auto">
                <div className="successMessage">
                  <b>Success!</b> An email sent to Osama Ahmed with your
                  message.
                </div>
                <h3 className="text-center mb-3">Get In Touch</h3>
                <div className="form-row">
                  <div className="form-group col-md-6">
                    <Field
                      name="name"
                      placeholder="Name"
                      type="text"
                      component={InputField}
                    />
                  </div>
                  <div className="form-group col-md-6">
                    <Field
                      name="email"
                      placeholder="Email"
                      type="email"
                      component={InputField}
                    />
                  </div>
                  <div className="form-group col-md-12">
                    <Field
                      name="message"
                      placeholder="Enter your message"
                      componentType="textarea"
                      rows={10}
                      component={InputField}
                    />
                  </div>
                  <div className="form-group col-md-12">
                    <button
                      type="submit"
                      disabled={isSubmitting}
                      className="btn btn-block btn-primary py-2"
                    >
                      Send It
                    </button>
                  </div>
                </div>
              </Form>
            )}
          />
        );
      }}
    </ContactComponent>
  );
};

export default ContactForm;
