import React from "react";
import MyLink from "./MyLink";

const CTAFooter = () => {
  return (
    <section className="section-cta py-5 bg-primary text-white">
      <div className="container">
        <div className="text-center">
          <img
            className="profile-small d-inline-block mx-auto rounded-circle mb-2"
            src={require("../assets/images/profile.jpg")}
            alt=""
          />
        </div>
        <h3 className="section-title font-weight-bold text-center mb-2">
          Interested in hiring me for your project?
        </h3>
        <div className="section-intro mx-auto text-center mb-3">
          Looking for an experienced full-stack developer to build your web,
          mobile or ship your software product? To start an initial chat, just
          drop me an email at{" "}
          <a
            className="link-on-bg"
            href="mailto:osama.ahmed220@gmail.com"
            target="_top"
          >
            osama.ahmed220@gmail.com
          </a>{" "}
          or use the{" "}
          <MyLink className="link-on-bg" href="contact">
            form on the contact page
          </MyLink>
          .
        </div>
        <div className="text-center">
          <MyLink className="theme-btn-on-bg btn" href="contact">
            Let's Talk
          </MyLink>
        </div>
      </div>
    </section>
  );
};

export default CTAFooter;
