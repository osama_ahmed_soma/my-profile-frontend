import React from "react";
import { FieldProps } from "formik";

const InputArea: React.SFC<any> = (
  props: React.DetailedHTMLProps<
    React.InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  >
) => {
  return <input {...props} />;
};

const TextArea: React.SFC<any> = (
  props: React.DetailedHTMLProps<
    React.TextareaHTMLAttributes<HTMLTextAreaElement>,
    HTMLTextAreaElement
  >
) => {
  return <textarea {...props} />;
};

interface InputFieldI {
  label?: string;
  componentType?: string;
}

const InputField: React.SFC<FieldProps<any> & InputFieldI> = ({
  field: { onChange, ...field },
  form: { touched, errors }, // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
  label,
  componentType = "text",
  ...props
}) => {
  const errorMsg = touched[field.name] && errors[field.name];
  const Comp = componentType === "textarea" ? TextArea : InputArea;
  return (
    <React.Fragment>
      <Comp
        {...field}
        className={`form-control ${errorMsg !== undefined ? "error" : ""}`}
        {...props}
        onChange={onChange}
      />
      {errorMsg !== undefined && (
        <label id="cname-error" className="error" htmlFor={field.name}>
          {errorMsg}
        </label>
      )}
    </React.Fragment>
  );
};

export default InputField;
