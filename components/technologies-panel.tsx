import React from "react";
import { SkillIdTitleFragment } from "../generated/apolloComponent";

interface Props {
  isLight?: boolean;
}

const TechnologiesPanel: React.SFC<SkillIdTitleFragment & Props> = ({
  title,
  isLight
}) => {
  return (
    <li className="list-inline-item">
      <span
        className={
          "badge " + (isLight ? "badge-light" : "badge-secondary badge-pill")
        }
        style={{ whiteSpace: title.length > 35 ? "normal" : "nowrap" }}
      >
        {title}
      </span>
    </li>
  );
};

export default TechnologiesPanel;
